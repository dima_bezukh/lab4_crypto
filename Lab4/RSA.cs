﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Lab4
{
    public class Public_key
    {
        public BigInteger modulus;
        public BigInteger exponent;
        public Public_key(BigInteger a, BigInteger b)
        {
            modulus = a;
            exponent = b;
        }
        public override string ToString()
        {
            return modulus.ToString()+ "\n" + exponent.ToString();
        }
    }
    public class Private_key
    {
        public BigInteger modulus;
        public BigInteger exponent;
        public BigInteger modulus_p;
        public BigInteger modulus_q;
        public Private_key(BigInteger mod, BigInteger exp)
        {
            modulus = mod;
            exponent = exp;
            modulus_p = 0;
            modulus_q = 0;
        }
        public override string ToString()
        {
            return modulus.ToString() + "\n" + exponent.ToString();
        }
    };
    public class RSA
    {
        public static string exp_constant = "17";

        public BigInteger ExtEuclid(BigInteger a, BigInteger b)
        {
            BigInteger x = 0, y = 1, u = 1, v = 0, gcd = b, m, n, q, r;
            while (!(a == 0))
            {
                q = gcd / a;
                r = gcd % a;
                m = x - u * q;
                n = y - v * q;
                gcd = a;
                a = r;
                x = u;
                y = v;
                u = m;
                v = n;
            }
            return y;
        }
        public void Generate_Keys(out Public_key pub, out Private_key priv, BigInteger p_prime, BigInteger q_prime)
        {
            BigInteger e = BigInteger.Parse(exp_constant);
            BigInteger N = BigInteger.Multiply(p_prime, q_prime);
            BigInteger phi_N = BigInteger.Multiply(p_prime - 1, q_prime - 1);
            BigInteger d = ExtEuclid(phi_N, e);
            while(d<0)
            {
                d += phi_N;
            }
            pub = new Public_key(N, e);
            priv = new Private_key(N, d);
            priv.modulus_p = p_prime;
            priv.modulus_q = q_prime;
        }
        public BigInteger Encrypt(BigInteger message, Public_key key)
        {
            return BigInteger.ModPow(message, key.exponent, key.modulus);
        }
        public BigInteger Decrypt(BigInteger encrypt, Private_key key)
        {
            BigInteger d_p = key.exponent % (key.modulus_p - 1);
            BigInteger d_q = key.exponent % (key.modulus_q - 1);
            BigInteger decrypted_p = BigInteger.ModPow(encrypt, d_p, key.modulus_p);
            BigInteger decrypted_q = BigInteger.ModPow(encrypt, d_q, key.modulus_q);
            BigInteger qInv = ExtEuclid(key.modulus_q, key.modulus_p);
            BigInteger h = (qInv * (decrypted_p - decrypted_q)) % key.modulus_p;
            BigInteger decrypted = decrypted_q + (h * key.modulus_q);
            return decrypted;
        }
    }
}
