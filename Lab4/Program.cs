﻿using System;
using System.Numerics;
using System.Text;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            string message = "Hello world!";
            string p_1024bit ="68294352699746087495779696363795722876293313715392875165012445907625670044697604360524935005484366717982111393882259962396153338121845354410187332953396839926911982813365078395384298409832523888620909736779078404299702782398228521137688807266000140851790839626232442247871402032080794194656157425305414058391";
            string q_1024bit ="61662749655758860915631655952712709426773858082836912491818190233963827778818425702202253495510629666725773583349710487791018208161179127035566823668051336683111831675543423032610358479602991619105405756176161018574802046643941246304815753577438325598698136521701159212121012787505683636828775869075877935657";

            string p_512bit = "631264565180476132207928240549299331370135074875535196682523118767376005341849248780027468027792844995209101631682168421004892745833400021468667641930919";
            string q_512bit = "6229425811746705010412387199640008636722253243772399153087352198602761530564040652079735814517762508851897276186177362851729935712298128079315260231948521";
            //Console.WriteLine(RabinMiller.IsPrime(BigInteger.Parse(q_512bit), 5));
            var watch = System.Diagnostics.Stopwatch.StartNew();
            RSA rsa = new RSA();
            Public_key public_Key;
            Private_key private_Key;
            //rsa.Generate_Keys(out public_Key, out private_Key, BigInteger.Parse(p_512bit), BigInteger.Parse(q_512bit));
            rsa.Generate_Keys(out public_Key, out private_Key, BigInteger.Parse(p_1024bit), BigInteger.Parse(q_1024bit));

            //Console.WriteLine(message);
            BigInteger encrypt = rsa.Encrypt(new BigInteger(Encoding.ASCII.GetBytes(message)), public_Key);
            BigInteger decrypt = rsa.Decrypt(encrypt, private_Key);
            Console.WriteLine(encrypt);
            Console.WriteLine(decrypt);
            Console.WriteLine(Encoding.Default.GetString(decrypt.ToByteArray()));
            watch.Stop();
            Console.WriteLine($"Time: {watch.Elapsed}\n");

            watch = System.Diagnostics.Stopwatch.StartNew();
            RSA_OAEP oaep = new RSA_OAEP(BigInteger.Parse(p_1024bit), BigInteger.Parse(q_1024bit));
            oaep.Proccess(message);
            watch.Stop();
            Console.WriteLine($"Time: {watch.Elapsed}");
        }
    }
}
